import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import AccueilView from './components/Accueil';
import NavbarView from './components/Navbar';
import NbCitationsView from './components/NbCitations';
import CitationsPersoView from './components/CitationsPersonnage';
import DetailsView from './components/DetailsView'

export default function App() {

  function Accueil() {
    return(
      <View>
          <AccueilView></AccueilView>
      </View>
    )
  }

  function NbCitations() {
    return(
      <View>
          <NbCitationsView></NbCitationsView>
      </View>
    )
  }

  function CitationPerso() {
    return(
      <View>
        <CitationsPersoView></CitationsPersoView>
      </View>
    )
  }

  function Details() {
    return(
      <View>
        <DetailsView></DetailsView>
      </View>
    )
  }

  const Stack = createNativeStackNavigator();

  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={Accueil} />
        <Stack.Screen name="NbCitations" component={NbCitations} />
        <Stack.Screen name="CitationPerso" component={CitationPerso} />
        <Stack.Screen name="Details" component={Details} />
      </Stack.Navigator>
      <NavbarView></NavbarView>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
