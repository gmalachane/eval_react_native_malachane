import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Image, ScrollView, Pressable, TextInput, Button, FlatList } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React, {useEffect, useState} from 'react'; 
import QuoteCardView from './QuoteCard';

const CitationsPersoView = () => {

    const [data, setData] = useState([]);
    const [Perso, setPerso] = useState();
    
    const fetchNbCitations = (perso) => {
    fetch(`https://thesimpsonsquoteapi.glitch.me/quotes?count=15&character=${Perso}`).then(res => res.json()).then(data => {
    setData(data);
    console.log("CitationsPerso: ", data);
    })
    }

    const handleClick = () => {
        console.log("handleClick ", Perso)
        fetchNbCitations();
    }

    const _renderItem = ({ item }) => <View><QuoteCardView data={item}></QuoteCardView></View>

    return(
        <View>
            <Text>Veuillez saisir un personnage :</Text>
            <TextInput 
            onChangeText={(newPerso) => setPerso(newPerso)}
            value={Perso}
            />
            <Pressable style={styles.rechercher} onPress={() => handleClick()}><Text>chercher</Text></Pressable>
            { data ? 
            <FlatList style={styles.list} data={data} renderItem={_renderItem}></FlatList>
            :
            <View/>
        }
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
      },
    rechercher: {
        backgroundColor: 'yellow',
        borderRadius: 4,
        borderWidth: 1,
        borderColor: 'black',
        padding: 5,
        marginTop: 20,
        color: 'white',
    }, 
    list: {
        alignSelf: 'center',
    }
  });

export default CitationsPersoView;