import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Image, ScrollView, Pressable, TextInput, Button, FlatList } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React, {useEffect, useState} from 'react'; 
import QuoteCardView from './QuoteCard';

const NbCitationsView = () => {

    const [data, setData] = useState([]);
    const [Nb, setNb] = useState(null);

    // useEffect(() => {
    //         fetchNbCitations();
    //   }, [])
    
    
    const fetchNbCitations = () => {
        console.log(Nb);
        fetch(`https://thesimpsonsquoteapi.glitch.me/quotes?count=${Nb}`).then(res => res.json()).then(data => {
        setData(data);
        console.log("NbCitations: ", data);
        })
    }

    const handleClick = () => {
        console.log("handleClick ", Nb)
        fetchNbCitations();
    }
    const _renderItem = ({ item }) => <View><QuoteCardView data={item}></QuoteCardView></View>

    return(
        <View style={styles.containernb}>
            <Text>Veuillez saisir un nombre :</Text>
            <TextInput 
            onChangeText={(newNb) => setNb(newNb)}
            value={Nb} 
            />
            <Pressable style={styles.rechercher} onPress={() => handleClick()}><Text>chercher</Text></Pressable>
            { data ? 
            <FlatList style={styles.list} data={data} renderItem={_renderItem}></FlatList>
            :
            <View/>
        }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
      },
    rechercher: {
        backgroundColor: 'yellow',
        borderRadius: 4,
        borderWidth: 1,
        borderColor: 'black',
        padding: 5,
        marginTop: 20,
        color: 'white',
    }, 
    list: {
        alignSelf: 'center',
    }
  });

export default NbCitationsView;