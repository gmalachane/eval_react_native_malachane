import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Image, ScrollView, Pressable } from 'react-native';
import { NavigationContainer, useNavigation } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React, {useEffect, useState} from 'react'; 

const QuoteCardView = (props) => {
    const navigation = useNavigation();
    const data = props.data

    return(
        <View style={styles.quoteCard}>
            <Text style={styles.character}>{data.character}</Text>
            <Text style={styles.quote}>{data.quote}</Text>
            <Pressable onPress={() => {navigation.navigate('Details', { data : data })}}><Image style={styles.img} source={{ uri : data.image, }}/></Pressable>
        </View>
    )
}

const styles = StyleSheet.create({
    quoteCard: { 
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
      borderColor: 'black',
      borderWidth: 1,
      borderRadius: 20,
      width: 250,
      padding: 10,
      marginBottom: 10,

    },
    quote: {
        marginBottom: 10,
    },
    character: {
        marginBottom: 10,
    },
    img: {
        height: 150,
        width: 150,
    }
  });

export default QuoteCardView;