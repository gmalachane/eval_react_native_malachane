import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Image, ScrollView, Pressable } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React, {useEffect, useState} from 'react'; 
import QuoteCardView from './QuoteCard';

const AccueilView = () => {

    const [data, setData] = useState([]);
    
    useEffect(() => {
      (async () => {
        fetchCitation();
      })();
    }, [])
  
    const fetchCitation = () => {
      fetch('https://thesimpsonsquoteapi.glitch.me/quotes').then(res => res.json()).then(data => {
      setData(data[0]);
      console.log("citation: ", data[0].image);
      })
    }
    
    return(
        <ScrollView>
            <View style={styles.containerHome}>
                <QuoteCardView data={data}></QuoteCardView>
                <Pressable style={styles.reload} onPress={fetchCitation}><Text>reload</Text></Pressable>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    containerHome: {
      flex: 1,  
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
      height: 700,
    },
    img: {
        height: 200,
        width: 200,
    },
    reload: {
        backgroundColor: 'yellow',
        borderRadius: 4,
        borderWidth: 1,
        borderColor: 'black',
        padding: 5,
        marginTop: 50,
        color: 'white',
    }
  });

export default AccueilView;